const {modCompPrices, selectComps} = require('../models/companyModels');
const {patchComps, getComps} = require('../controllers/companyControllers');

exports.testFunc = async (req, res) => {
    console.log('got to testFunc');
    const comps = await selectComps(); 
    const modComps = modCompPrices(comps);
    return patchComps(modComps).then((comps) => {
      res.send({ comps });
    });
  };