exports.formatCompsData = (compsData) => {
    if (compsData === undefined) return [];
    return compsData.map((c) => {
      return [c.name, c.todaysSharePrice, c.yesterdaysSharePrice, c.amountSharesAvailiable, c.amountSharesPlayerHolds];
    });
  };