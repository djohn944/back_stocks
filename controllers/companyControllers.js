const {
  selectComps,
  selectCompanyById,
  modCompPrices,
  updateCompanyById,
} = require('../models/companyModels');
const db = require('../db/connection');
const res = require('express/lib/response');

exports.getComps = async (req, res) => {
  console.log('got here GetComps!!!!!');
  const comps = await selectComps(req.query);
  res.send({ comps });
};

exports.getCompById = async (req, res) => {
  const { company_id } = req.params;
  console.log('FRom get companyById: ', company_id);
  const company = await selectCompanyById(company_id);
  console.log('from getcompanybyID returnedCompany: ', company);
  res.status(200).send(company);
};

exports.patchCompanyById = async (req, res) => {
  const { company_id } = req.params;
  const { mod_price } = req.params;
  console.log('mod_price : ',mod_price);
  const company = await updateCompanyById(company_id, mod_price);
  res.send({ company });
};
