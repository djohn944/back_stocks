const db = require('../db/connection.js');
const testData = require('../db/data/test-data/index.js');
const { seed } = require('../db/seeds/seed.js');
const request = require('supertest');
const app = require('../app');

beforeEach(() => seed(testData));
afterAll(() => db.end());

describe('GET /', () => {
  test('tests basic root route - Hello World', () => {
    return request(app)
      .get('/')
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual({ msg: 'Hello World!' });
      });
  });
});

describe('GET /comps', () => {
  test('200: responds with an array', () => {
    return request(app)
      .get('/comps')
      .expect(200)
      .then((res) => {
        expect(res.body.comps).toBeInstanceOf(Array);
      });
  });
});

describe('GET /comp/:name', () => {
  test('200: responds with a Company when given its name', () => {
    return request(app)
      .get('/comp/1')
      .expect(200)
      .then((res) => {
        console.log(res.body);
        expect(res.body).toEqual({
          company_id: 1,
          name: 'Beta Tomato Marketing',
          todaysSharePrice: 300.23,
          yesterdaysSharePrice: 303.45,
          amountSharesAvailiable: 100,
          amountSharesPlayerHolds: 0,
          pricePlayerBoughtShare: 0,
        });
      });
  });
});

describe('PATCH /comp:company_id', () => {
  test('200: responds with an array', () => {
    return request(app)
      .patch('/comp/1')
      .send(99)
      .expect(200)
      .then((res) => {
        console.log(res);
        expect(res.body.comps).toBeInstanceOf(Array);
      });
  });
});
