const patchComps = require('../controllers/companyControllers');
const db = require('../db/connection');
const { changePrices } = require('../utils/priceMods');

exports.selectComps = async (req, res) => {
  console.log('got to selectComps', req);
  return await db.query('SELECT * FROM comps;').then((res) => {
    return res.rows;
  });
};

exports.selectCompanyById = async (company_id) => {
  console.log('from selectCompany: ', company_id);

  const company = await db
    .query(`SELECT * FROM comps WHERE company_id=$1`, [company_id])
    .then((result) => result.rows[0]);
  console.log(company);
  return company;
};

exports.modCompPrices = async () => {
  // select comps
  return selectComps()
    .then((comps) => {
      const newComps = changePrices(comps);
      console.log(newComps);
      return patchComps(newComps);
    })
    .then((comps) => {
      console.log(comps);
    });
  // mod comps
  // patch comps
};

exports.updateCompanyById = async (company_id, mod_price) => {
  const company = await db
    .query(
      `UPDATE comps SET todaysSharePrice = $1 WHERE company_id = $2 RETURNING *;`,
      [mod_price, company_id]
    )
    .then((result) => result.rows[0]);

  if (!company) {
    return Promise.reject({
      status: 404,
      msg: `Company with id: ${company_id} not found`,
    });
  }

  return company;
};
