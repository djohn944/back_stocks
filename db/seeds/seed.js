const db = require('../connection');
const format = require('pg-format');
const { formatCompsData } = require('../../utils/formatData');

const seed = (data) => {
  const { companies } = data;

  return db
    .query(`DROP TABLE IF EXISTS comps cascade;`)
    .then(() => {
      return db.query(`DROP TABLE IF EXISTS history cascade;`);
    })
    .then(() => {
      return db.query(`
        CREATE TABLE comps (
          company_id SERIAL PRIMARY KEY,
          name TEXT,
          todaysSharePrice NUMERIC,
          amountSharesAvailiable NUMERIC,
          amountSharesPlayerHolds NUMERIC,
          pricePlayerBoughtShare NUMERIC
        );`);
    })
    .then(() => {
      return db.query(`
        CREATE TABLE history (
          hname TEXT,
          price NUMERIC,
          turn SERIAL 
        );`);
    })
    .then(() => {
      const formattedComps = formatCompsData(companies);
      const sql = format(
        `INSERT INTO comps (name, todaysSharePrice, amountSharesAvailiable, amountSharesPlayerHolds, pricePlayerBoughtShare) 
        VALUES %L RETURNING *;`,
        formattedComps
      );
      return db.query(sql);
    });
};

module.exports = { seed };
