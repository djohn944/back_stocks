module.exports = [
    {
      name: "Beta Tomato Marketing",
      todaysSharePrice: 300.23,
      yesterdaysSharePrice: 303.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
    
    },
    {
      name: "Cheeky Fox Films",
      todaysSharePrice: 303.53,
      yesterdaysSharePrice: 300.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
  
    },
    {
      name: "Alpha Book Builders",
      todaysSharePrice: 604.33,
      yesterdaysSharePrice: 603.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
 
    },
    {
      name: "Ice-Cold Squirrel Stocks",
      todaysSharePrice: 470.33,
      yesterdaysSharePrice: 408.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
    
    },
    {
      name: "Purple Robot Tech",
      todaysSharePrice: 400.33,
      yesterdaysSharePrice: 483.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
      
    },
    {
      name: "Black Fox Bank",
      todaysSharePrice: 788.33,
      yesterdaysSharePrice: 743.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
     
    },
    {
      name: "Tall Fan Pharma Company",
      todaysSharePrice: 404.33,
      yesterdaysSharePrice: 483.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
     
    },
    {
      name: "Rainy Pen Builders",
      todaysSharePrice: 656.33,
      yesterdaysSharePrice: 660.45,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
     
    },
    {
      name: "Happy Crab Marketing",
      todaysSharePrice: 781.01,
      yesterdaysSharePrice: 760.35,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
  
    },
    {
      name: "Blue Moose Inc",
      todaysSharePrice: 771.32,
      yesterdaysSharePrice: 770.35,
      amountSharesAvailiable: 100,
      amountSharesPlayerHolds: 0,
      pricePlayerBoughtShare: 0,
    
    },
  ];