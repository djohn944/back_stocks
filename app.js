const {getComps, getCompById, patchCompanyById} = require('./controllers/companyControllers');
const {testFunc} = require('./utils/testFunc');
const cors = require('cors');
const express = require('express');

const app = express();
app.use(cors());

app.get('/', (req, res) => res.status(200).send({msg: 'Hello World!'}));

app.get('/comps', getComps);

app.get('/comp/:company_id', getCompById);

app.patch('/comp/:company_id/:mod_price', patchCompanyById);

app.patch('/test', testFunc);

app.use(express.json());

app.options('*', cors());

module.exports = app;